import { Router } from "@reach/router"
import { useSelector } from "react-redux"
import LayoutCliente from "../../components/cliente/layout"
import { enumRole } from "../../util/roles"
import Error404 from "../error/error404"
import Home from "./home"
import Servicos from "./servicos"

const Menu = [
    {
        title: "Home",
        path: "/",
        component: Home,
        authorization: [3]
    },
    {
        title: "Meus serviços",
        path: "/servicos",
        component: Servicos,
        authorization: [3]
    }
]

const Cliente = ({location, uri, perfil}) => {
    document.title = perfil;
    const userRole = useSelector(state => state.auth.auth.user.role.type);
    const roleId = enumRole(userRole);

    const routersAuthorized = Menu.filter((route) => 
        route.authorization.includes(roleId)
    );

    const currentRoute = Menu.find(
        item => item.path === `/${location.pathname.split("/"[2] || "")}`
    );

    return (  
        <Router>
            <LayoutCliente 
                path="/" 
                tipoDeAcesso="CLIENTE"
                current={currentRoute}
                menu={routersAuthorized}
                uri={uri}
            >
                {routersAuthorized.map(({component: Component, ...route}, i) => (
                    <Component {...route} key={i}/>
                ))}
                <Error404 default />
            </LayoutCliente>
        </Router>
    )
};

export default Cliente;