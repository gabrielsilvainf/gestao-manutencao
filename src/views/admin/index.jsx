import { Router } from "@reach/router";
import { useSelector } from "react-redux";
import LayoutAdministrador from "../../components/administrador/layout";
import { enumRole } from "../../util/roles";
import Error404 from "../error/error404";
import Clientes from "./clientes";
import Funcionarios from "./funcionarios";
import Home from "./home";
import { UserOutlined } from "@ant-design/icons"

//entrada de dados de maneira dinamica
const Menu = [
    {
        title: "Home",
        path: "/",
        icon: <UserOutlined/>,
        component: Home,
        authorization: [1]
    },
    {
        title: "Lista de clientes",
        path: "/clientes",
        component: Clientes,
        authorization: [1]
    },
    {
        title: "Lista de funcionarios",
        path: "/funcionarios",
        component: Funcionarios,
        authorization: [1]
    }
];

const Admin = ({location, uri, perfil}) => {
    document.title = perfil;

    const userRole = useSelector(state => state.auth.auth.user.role.type);
    const roleId = enumRole(userRole);

    const routersAuthorized = Menu.filter((route) => 
        route.authorization.includes(roleId)
    );

    const currentRoute = Menu.find((item) => 
        item.path === `/${location.pathname.split("/")[2] || ""}`
    );

    return (   
        <Router>
            <LayoutAdministrador 
                path="/" 
                tipoDeAcesso={perfil || ""} 
                current={currentRoute}
                menu={routersAuthorized}
                uri={uri}
            >
                {routersAuthorized.map(({component: Component, ...route}, i) => (
                    <Component {...route} key={i}/>
                ))}
                <Error404 default/>
            </LayoutAdministrador>
        </Router>
        
    )
};

export default Admin;