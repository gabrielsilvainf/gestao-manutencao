import React, { useState } from "react";
import styled from "styled-components";
import {
  FormGroup,
  Input,
  Card,
  Col,
  CardBody,
  CardHeader,
  Button,
  CardFooter,
  Label
} from "reactstrap";
import { Link } from "@reach/router";
import { useDispatch, useSelector } from "react-redux";
import { authLogin } from "../../store/auth/action";
import { CircularProgress, Stack } from "@material-ui/core";

const Login = () => {
  const dispatch = useDispatch();
  const authLoading = useSelector((state) => {
    return state.auth.loading
  })

  const [form, setForm] = useState({
    identifier: "Administrador",
    password: "administradorsupremo",
  });

  const handlechange = (event) =>
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });

    const isNotValid = () => 
        Object.keys(form).length === 0 || 
        form.identifier?.length === 0 || 
        form.password?.length === 0;

    const hendleSubmit = () => dispatch(authLogin(form));


  return (
    <Sign>
      <Col sm={12} md={4} lg={5}>
        <Card>
          <CardHeader tag="h4" className="text-center">
            Login
          </CardHeader>
          <CardBody>
            <FormGroup className="my-3">
              <Label for="email">E-mail:</Label>
              <Input
                onChange={handlechange}
                type="email"
                name="identifier"
                id="usuario"
                placeholder="Informe seu E-mail"
                value={form.identifier || ""}
              />
            </FormGroup>
            <FormGroup className="my-3">
              <Label for="password">Senha:</Label>
              <Input
                onChange={handlechange}
                type="password"
                name="password"
                id="senha"
                placeholder="Informe sua senha"
                value={form.password || ""}
              />
            </FormGroup>
            <Button
              size="sm"
              block
              disabled={isNotValid()}
              color={isNotValid() ? "secondary": "primary"}
              onClick={hendleSubmit}
            >
              { authLoading ? <Stack>
                <CircularProgress
                  size={30}
                  thickness={4}
                  sx={{color: "#ffffff !important"}}
                />
              </Stack> : "Entrar" } 
            </Button>
          </CardBody>
          <CardFooter className="text-muted">
            Não tem Cadastro? <Link to="/cadastro">Cadastre-se</Link>
          </CardFooter>
        </Card>
      </Col>
    </Sign>
  );
};

export default Login;

const Sign = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 90vh;
`;