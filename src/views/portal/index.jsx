import { Router } from "@reach/router";
import LayoutPortal from "../../components/portal/layout";
import Login from "../auth/login";
import DetalhesServico from "./detalhesServico";
import Home from "./home";
import Loja from "./loja";
import Servico from "./servicos";

const Portal = () => {
    return (      
        <LayoutPortal>
            <Router>
                <Home path="/"/>
                <Servico path="/servicos"/>
                <DetalhesServico path="/servicos/:id"/>
                <Login path="/login"/>
                <Loja path="/loja"/>
            </Router>
        </LayoutPortal>
    );
};

export default Portal;