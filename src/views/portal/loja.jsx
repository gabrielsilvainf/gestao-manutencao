import { MenuItem } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "reactstrap";
import styled from "styled-components";
import CardProdutos from "../../components/portal/cardprodutos";
import SelecaoTipo from "../../components/portal/selecaotipo";
import { getProdutos } from "../../store/produtos/action";

const Loja = () => {
    const dispatch = useDispatch();

    const produtos = useSelector(state => state.produtos.todos);

    const [form, setForm] = useState({tipo: ""});

    const handlechange = (event) =>
        setForm({
            ...form,
            [event.target.name]: event.target.value,
        });

    useEffect(() => {
        dispatch(getProdutos());
        return () => {};
    }, [dispatch]);

    const todosProdutos = (lista) => {
        return lista.map((item) => {
            return (
                <CardProdutos
                    key={item.id}
                    title={item.title}
                    description={item.description}
                    image={"https://img.clasf.com.br/2020/08/06/Placa-De-Vdeo-Evga-Geforce-Gtx-1050-2-Gb-Usada-20200806124939.2002970015.jpg"}
                />
            );
        });
    }


    const pegarTipos = (lista) => {
        return lista.map((item, i) => {
            return (
                <MenuItem 
                    value={item.tipo}
                    key={i}
                >
                    
                    {item.tipo}
                </MenuItem>
            )
        })
    }

    return (
        <>
        <Container>
            <h2>
                Aqui vendemos peças usadas, que pertenciam aos nossos clientes
                e ainda estão em funcionamento.
            </h2>
            <h4>Garantimos que essas peças não eram usadas para mineração</h4>
        </Container>
        <Container>
            <SelecaoTipo 
                tipo={form.tipo}
                handlechange={handlechange}
            >
            {pegarTipos(produtos)}

            </SelecaoTipo>
        </Container>
        <Container>
            <ServicoContainer>
                {todosProdutos(produtos)}
            </ServicoContainer>
        </Container>
        </>
    );
}

export default Loja;

const ServicoContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 20px;
    padding: 30px 20px;
`;