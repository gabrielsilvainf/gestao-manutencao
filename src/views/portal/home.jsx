import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "reactstrap";
import styled from "styled-components";
import CardAvaliacao from "../../components/portal/cardavaliacao";
import Banner from "../../components/portal/banner";
import CarrosselCards from "../../components/portal/carrosselcards";
import TresManutencao from "../../components/portal/conteudos/tresmanutencoes";
import { getAvaliacoes } from "../../store/avaliacoes/action";

const Home = () => {

    const dispatch = useDispatch();
    const avaliacoes = useSelector( state => state.avaliacoes?.todos);

    useEffect(() => {
        dispatch(getAvaliacoes());
        return(() => {});
    }, [dispatch])

    return (
        <div>
            <ContainerBox>
                <Container>
                    <Banner/>
                </Container>
            </ContainerBox>
            <ContainerBoxBranco>
                <Container>
                    <TresManutencao/>
                </Container>
            </ContainerBoxBranco>
            <ContainerBox>
                <Container>
                    <SContainer>
                    <h3>Quem já usou nosso serviço não teve arrependimentos</h3>
                        <div 
                            style={{ maxWidth: 1200, marginLeft: 'auto', marginRight: 'auto', marginTop: 64 }}>
                            <CarrosselCards
                                show={4}
                            >
                                {avaliacoes?.map((item) => (
                                    <CardAvaliacao
                                        key={item.id}
                                        nome={item.nomeUsuario}
                                        data={item.published_at}
                                        imagem={'https://static.wixstatic.com/media/80a5e6_8a38e1574a5644f894fb781521496548~mv2.jpg/v1/fill/w_498,h_332,al_c,q_85,usm_0.66_1.00_0.01/80a5e6_8a38e1574a5644f894fb781521496548~mv2.jpg'}
                                        descricao={item.descricao}
                                        nota={item.nota}
                                    />
                                ))}
                            </CarrosselCards>
                        </div>
                    </SContainer>
                </Container>
            </ContainerBox>
        </div>
    );
};

export default Home;

const ContainerBox = styled.div`
    background-color: #5C66D2;
`;

const ContainerBoxBranco = styled.div`
    background-color: #fff;
`;

const SContainer = styled.div`
    padding: 50px 0;

    h3{
        color: #ffffff;
    }
`;