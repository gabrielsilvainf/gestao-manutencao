import { navigate } from "@reach/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "reactstrap";
import styled from "styled-components";
import CardServico from "../../components/portal/cardservico"
import { getServicos, servicoSelecionado } from "../../store/servicos/action";

const Servico = () => {

    const dispatch = useDispatch();

    const servicos = useSelector(state => state.servicos.todos);

    useEffect(() => {
        dispatch(getServicos());
        return (() => {});
    }, [dispatch]);

    const goToServiceById = (servico) => {
        dispatch(servicoSelecionado(servico)).then(() => navigate(`servicos/${servico.id}`));
    }

    const mostrarServicos = (lista) => (
        lista.map(servico => (
            <CardServico
                key={servico.id}
                data={servico}
                goToServiceById={goToServiceById}
            />
        ))
    );

    return (
        <Container>
            <ServicoContainer>
                {mostrarServicos(servicos)}
            </ServicoContainer>
        </Container>
    );
};

export default Servico;

const ServicoContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 20px;
    padding: 20px;
`;