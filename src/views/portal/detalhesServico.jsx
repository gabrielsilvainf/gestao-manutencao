import { useEffect } from "react";
import { Container } from 'reactstrap';
import { useDispatch, useSelector } from "react-redux";
import { getServicoById } from "../../store/servicos/action";
import * as R from "ramda";

const DetalhesServico = ({id}) => {

    const dispatch = useDispatch();
    const servicoSelecionado = useSelector(state => state.servicos.selecionado);

    useEffect(() => {
        if(R.isEmpty(servicoSelecionado)){
            dispatch(getServicoById(id));
        }
    }, [dispatch, id, servicoSelecionado]);

    return (
        <Container>
            <h1>{servicoSelecionado.title}</h1>
            <p>{servicoSelecionado.description}</p>
        </Container>
    );
}

export default DetalhesServico;