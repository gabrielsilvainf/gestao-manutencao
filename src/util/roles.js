export const enumRole = (role) => {
    const mapRole = {
        'authenticated': 1,
        'funcionario': 2,
        'usuario': 3
    }

    return mapRole[role];
};