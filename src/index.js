import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import GlobalStyled from './assets/globalStyled';
import Routers from './routers';
import store from "./store";
import { ToastContainer } from 'react-toastify';

import "./config/bootstrap";

ReactDOM.render(
  <Provider store={store}>
    <Routers/>
    <GlobalStyled/>
    <ToastContainer/>
  </Provider>,
  document.getElementById('root')
);