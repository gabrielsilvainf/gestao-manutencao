import { Button, Menu } from "antd"
import Layout, { Content, Header } from "antd/lib/layout/layout"
import Sider from "antd/lib/layout/Sider"
import styled from "styled-components";
import { useSelector } from "react-redux";
import { Link, navigate } from "@reach/router";
import { removeUser } from "../../../config/storage";

const LayoutAdministrador = ({children, tipoDeAcesso, current, menu, uri}) => {
    const auth = useSelector(state => state.auth.auth);

    const logout = () => {
        navigate("/");
        removeUser();
    }
    
    return (
        <SLayout>
            <Sider trigger={null} collapsible>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                    {menu?.map((item, i) => (
                        <Menu.Item key={i} icon={item.icon}>
                            <Link to={uri + item.path}>{item.title}</Link>
                        </Menu.Item>
                    ))}
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }}>
                    {auth.user.username}
                    <Button onClick={logout}>Logout</Button>
                </Header>
                <Content>
                    <h4>{current.title}</h4>
                    {children}
                </Content>
            </Layout>
        </SLayout>
    );
};

export default LayoutAdministrador;

const SLayout = styled(Layout)`
    height: 100vh;
`;