import { 
    Card,
    CardMedia, 
    CardContent, 
    CardActions,
    Typography,
    Button
} from '@material-ui/core';

import { 
    FavoriteBorder,
    AddShoppingCart
} from '@material-ui/icons';

const CardProdutos = ({title, description, image}) => {
    return (
        <Card>
            <CardMedia
                component="img"
                height="200"
                image={image}
                alt={title}
            />
            <CardContent>
                <Typography 
                    gutterBottom 
                    variant="h6" 
                    component="div"
                >
                    {title}
                </Typography>
                <Typography
                    variant="body2"
                    color="text.secondary"
                >
                    {description}
                </Typography>
            </CardContent>
            <CardActions>
                <Button><FavoriteBorder/></Button>
                <Button><AddShoppingCart/></Button>
                <Button>Ver mais</Button>
            </CardActions>
        </Card>
    )
}

export default CardProdutos;