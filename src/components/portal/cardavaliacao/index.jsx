import { 
    CardHeader, 
    Card, 
    Avatar, 
    CardMedia, 
    CardContent, 
    CardActions,
    Rating,
    Typography
} from '@material-ui/core';

const CardAvaliacao = ({nome, data, descricao, imagem, nota}) => {
    return (
        <Card sx={{ maxWidth: 345 }}>
            <CardHeader
                avatar={
                    <Avatar style={{background: 'red'}} aria-label="recipe">
                    R
                    </Avatar>
                }
                title={nome}
                subheader={data}
            />
            <CardMedia
                component="img"
                height="194"
                image={imagem}
                alt="resultado"
            />
            <CardContent>
                <Typography variant="body2" color="text.secondary">
                    {descricao}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <div>
                    <Typography component="legend">Avaliação</Typography>
                    <Rating
                        name="simple-controlled"
                        value={parseInt(nota)}
                        readOnly
                    />
                </div>
            </CardActions>
        </Card>
    );
}

export default CardAvaliacao;