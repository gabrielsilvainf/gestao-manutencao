import { Link } from '@reach/router';
import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import styled from 'styled-components';

const NavBar = (props) => {

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <SNavbar dark expand="md">
        <Brand tag={Link} to="/">Manutenção S.A.</Brand>
        <NavbarToggler onClick={toggle} />
        <SCollapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <Item>
              <NavLink tag={Link} to="/">Home</NavLink>
            </Item>
            <Item>
              <NavLink tag={Link} to="/servicos">Serviços</NavLink>
            </Item>
            <Item>
              <NavLink tag={Link} to="/loja">Loja</NavLink>
            </Item>
            <Item>
              <NavLink tag={Link} to="/sobre">Sobre</NavLink>
            </Item>
            <ItemLogin>
              <NavLink tag={Link} to="/login">Login</NavLink>
            </ItemLogin>
          </Nav>
        </SCollapse>
      </SNavbar>
    </div>
  );
}

export default NavBar;

const SNavbar = styled(Navbar)`
  padding: 2px !important;
`;

const Brand = styled(NavbarBrand)`
  flex: 1;
  font-weight: bold;
`;

const Item = styled(NavItem)`
a{
    font-weight: bold;
    color: #fff !important;
    margin: 5px;
    :hover{
        color: #00FF99 !important;
    }
  }
`;

const ItemLogin = styled(NavItem)`
  a{
      font-weight: bold;
      color: #fff !important;
      border-radius: 5px;
      background-color: #00CC99;
      padding-left: 20px !important;
      padding-right: 20px !important;
      margin: 5px;
      :hover{
          background-color: #00FF99;
          color: #5c66d2 !important;
      }
    }
`;

const SCollapse = styled(Collapse)`
  flex: 0;
`;