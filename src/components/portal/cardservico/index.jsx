
import React from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';
import styled from 'styled-components';

const CardServico = ({data, goToServiceById}) => {
  return (
    <SCard>
        <SCardBody>
            <CardTitle tag="h5">{data.title}</CardTitle>
            <CardSubtitle tag="h6" className="mb-2 text-muted">
            {data.minimumValue ? data.minimumValue: data.value}
            </CardSubtitle>
            <CardText>{data.frontDescription}</CardText>
            <SButton 
              size="sm" 
              outline color="info"
              onClick={() => goToServiceById(data)}
            >Saiba mais...</SButton>
        </SCardBody>
    </SCard>
  );
};

export default CardServico;

const SCard = styled(Card)`
    align-items: stretch;
`;

const SCardBody = styled(CardBody)`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

const SButton = styled(Button)`
  width: 50%;
  :hover{
    color: #fff;
  }
`;