import { Affix } from "antd";
import styled from "styled-components";
import Footer from "./footer";
import Header from "./header";

const LayoutPortal = ({children}) => {
    return (
        <LayoutContainer>
            <Affix offsetTop={0}><Header/></Affix>
                <Main>{children}</Main>
            <Footer/>
        </LayoutContainer>
    )
};

export default LayoutPortal;

const LayoutContainer = styled.div`
    display: flex;
    height: 100vh;
    flex-direction: column;
`;

const Main = styled.main`
    flex:1;
`;