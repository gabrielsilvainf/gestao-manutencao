import { Container } from "reactstrap";
import styled from "styled-components";
import NavBar from "../navbar";

const Header = () => {
    return (
        <HeaderBox>
            <Container>
                <NavBar/>
            </Container>
        </HeaderBox>
    );
}

export default Header;

const HeaderBox = styled.header`
    background-color: #5C66D2;
`;