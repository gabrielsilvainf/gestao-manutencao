import { Container } from "reactstrap";
import styled from "styled-components";

const Footer = () => {
    return (
        <FooterBox>
            <Container>
                Direitos reservados a Gabriel Carvalho
            </Container>
        </FooterBox>
    );
}

export default Footer;

const FooterBox = styled.footer`
    color: #fff;
    text-align: center;
    background-color: #1c152d;
    padding: 2px;
`;