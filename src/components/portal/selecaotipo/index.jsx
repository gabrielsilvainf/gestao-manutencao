import {
    Select,
    FormControl,
    InputLabel
} from '@material-ui/core'

const SelecaoTipo = ({children, tipo, handlechange}) => {
    
  
    return (
      <div>
        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="demo-simple-select-standard-label">Tipo</InputLabel>
          <Select
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            value={tipo}
            onChange={handlechange}
            label="Tipo"
            name="tipo"
          >
            {children}
          </Select>
        </FormControl>
      </div>
    );
  }

export default SelecaoTipo;