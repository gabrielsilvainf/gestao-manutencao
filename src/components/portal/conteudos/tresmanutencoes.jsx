import { navigate } from "@reach/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { getServicos, servicoSelecionado } from "../../../store/servicos/action";
import CardServico from "../cardservico";

const TresManutencao = () => {
    const dispatch = useDispatch();
    const servicos = useSelector(state => state.servicos?.todos);

    useEffect(() => {
        dispatch(getServicos());
        return (() => {});
    }, [dispatch]);

    const goToServiceById = (servico) => {
        dispatch(servicoSelecionado(servico)).then(() => navigate(`servicos/${servico.id}`));
    }

    const filtroServicos = (lista) => {
        const filtrado = lista.filter(item => item.id < 4);

        return filtrado.map(item => (
            <CardServico 
                key={item.id}
                data={item}
                goToServiceById={goToServiceById}
            />
        ))
    }

    return (
        <SContainer>
            <h3>Aqui resolvemos seus problemas!</h3>
            <ServicoContainer>
                {filtroServicos(servicos)}
            </ServicoContainer>
        </SContainer>
    );
}

export default TresManutencao;

const SContainer = styled.div`
    padding: 50px 0;
`;

const ServicoContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 20px;
    padding: 30px 20px;
`;