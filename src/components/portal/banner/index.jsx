import { navigate } from '@reach/router';
import React from 'react';
import { Jumbotron, Button } from 'reactstrap';
import styled from 'styled-components';
import fundo from "../../../assets/img/fundo-banner-3.jpg";

const Banner = (props) => {
  return (
    <div>
      <SJumbotron>
        <h1 className="display-5" style={{color: "#fff"}}>Seja bem vindo a Manutenção S.A.</h1>
        <p className="lead">A maior empresa de manutenção de Computadores de todo território brasileiro Caso seu computador esteja precisando de limpeza de equipamento, concerto ou até mesmo uma simples formatação de memória, conte conosco!</p>
        <p className="lead">venha conhecer nossos serviços</p>
        <p className="lead">
            <Button 
                color="outline-light" 
                size="sm"
                onClick={() => navigate("/servicos")}
            >
            Serviços</Button>
        </p>
      </SJumbotron>
    </div>
  );
};

export default Banner;

const SJumbotron = styled(Jumbotron)`
  padding: 400px 10px 100px;
  color: #fff;
  background-image: url(${fundo});
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
`;
