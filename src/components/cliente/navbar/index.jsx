import { AccountCircle } from '@material-ui/icons';
import { Link } from '@reach/router';
import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap';
import styled from 'styled-components';


const NavBarCliente = (props) => {

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <SNavbar dark expand="md">
        <Brand>Manutenção S.A.</Brand>
        <NavbarToggler onClick={toggle} />
        <SCollapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <Item>
              <NavLink>Home</NavLink>
            </Item>
            <Item>
              <NavLink>Serviços</NavLink>
            </Item>
            <Item>
              <NavLink>Loja</NavLink>
            </Item>
            <Item>
              <NavLink>Carrinho</NavLink>
            </Item>
            <ItemLogin>
              <UncontrolledDropdown setActiveFromChild>
                <DropdownToggle 
                  tag="a" 
                  className="nav-link"
                >
                  <AccountCircle/>
                </DropdownToggle>
                
                <DropdownMenu
                  right
                  modifiers={{
                    setMaxHeight: {
                      enabled: true,
                      order: 890,
                      fn: (data) => {
                        return {
                          ...data,
                          styles: {
                            ...data.styles,
                            overflow: 'none',
                            maxHeight: 'auto',
                          },
                        };
                      },
                    },
                  }}
                >
                  <DropdownItem header>
                    Nome do usuario
                  </DropdownItem>

                  <DropdownItem>Link</DropdownItem>
                  <DropdownItem>Link</DropdownItem>
                  <DropdownItem>Logout</DropdownItem>

                </DropdownMenu>
              </UncontrolledDropdown>
            </ItemLogin>
          </Nav>
        </SCollapse>
      </SNavbar>
    </div>
  );
}

export default NavBarCliente;

const SNavbar = styled(Navbar)`
  padding: 2px !important;
`;

const Brand = styled(NavbarBrand)`
  flex: 1;
  font-weight: bold;
`;

const Item = styled(NavItem)`
a{
    font-weight: bold;
    color: #fff !important;
    margin: 5px;
    :hover{
        color: #00FF99 !important;
    }
  }
`;

const ItemLogin = styled(NavItem)`
  a{
      font-weight: bold;
      color: #fff !important;
      border-radius: 5px;
      background-color: #00CC99;
      padding-left: 20px !important;
      padding-right: 20px !important;
      margin: 5px;
      :hover{
          background-color: #00FF99;
          color: #5c66d2 !important;
      }
    }
`;

const SCollapse = styled(Collapse)`
  flex: 0;
`;