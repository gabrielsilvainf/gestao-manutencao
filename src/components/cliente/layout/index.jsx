import { Affix, Menu } from "antd"
import Layout, { Content, Header } from "antd/lib/layout/layout"
import Sider from "antd/lib/layout/Sider"
import styled from "styled-components";
import { Container } from "reactstrap";
import{ UserOutlined } from "@ant-design/icons"
import NavBarCliente from "../navbar";

const LayoutCliente = ({children, tipoDeAcesso}) => {
    return (
        <LayoutContainer>
            <HeaderBox>
                <Container>
                    <Affix offsetTop={0}>
                        <NavBarCliente/>
                    </Affix>
                </Container>
                
                </HeaderBox>
            
                <Main>
                    {children}
                </Main>

                <footer>
                    <p>não esquecer de por o footer</p>
                </footer>
        </LayoutContainer>
    );
};

export default LayoutCliente;

const LayoutContainer = styled.div`
    display: flex;
    height: 100vh;
    flex-direction: column;
`;

const Main = styled.main`
    flex:1;
`;

const HeaderBox = styled.header`
    background-color: #5C66D2;
`;