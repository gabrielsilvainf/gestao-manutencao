import { Menu } from "antd"
import Layout, { Content, Header } from "antd/lib/layout/layout"
import Sider from "antd/lib/layout/Sider"
import styled from "styled-components";
import { UserOutlined } from "@ant-design/icons"

const LayoutFuncionarios = ({children}) => {
    return (
        <SLayout>
            <Sider trigger={null} collapsible>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                    <Menu.Item icon={<UserOutlined />}>1</Menu.Item>
                    <Menu.Item>2</Menu.Item>
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background" style={{ padding: 0 }}>header</Header>
                <Content>{children}</Content>
            </Layout>
        </SLayout>
    );
};

export default LayoutFuncionarios;

const SLayout = styled(Layout)`
    height: 100vh;
`;