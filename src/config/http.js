import axios from "axios";
import { getToken } from "./storage";

const { REACT_APP_API: api } = process.env;

const http = axios.create({
    baseURL: api,
});

if(getToken()){
    http.defaults.headers['Authorization'] = `bearer ${getToken().jwt}`;
}


http.defaults.headers["content-type"] = "application/json";

export default http;