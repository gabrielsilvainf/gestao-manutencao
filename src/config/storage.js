const {REACT_APP_TOKEN_KEY: tk} = process.env;

export const saveAuth = (data) => localStorage.setItem(tk, JSON.stringify(data));

export const getToken = () => JSON.parse(localStorage.getItem(tk));

export const removeUser = () => {
    window.location.reload(true);
    return localStorage.removeItem(tk);
};

export const clearStorage = () => localStorage.clear();

export const hasToken = () => getToken()?.jwt || false;

export const isAuthenticated = () => hasToken() !== false;