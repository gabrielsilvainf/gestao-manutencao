import { SERVICOS } from "../types";

const INITIAL_STATE = {
    todos: [],
    selecionado: {}
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type){
        case SERVICOS.getALL:
            state.todos = action.servicos;
            return state;
        case SERVICOS.select:
            state.selecionado = action.servicos;
            return state;
        default:
            return state;
    }
};

export default reducer;