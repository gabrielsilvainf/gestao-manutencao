import { getServicoService, getServicoByIdService } from "../../services/servicos.service";
import { SERVICOS } from "../types";

export const getServicos = () => {
    return async (dispatch) => {
        const result = await getServicoService();
        dispatch({ type: SERVICOS.getALL, servicos: result.data });
    }
}

export const servicoSelecionado = (servico) => {
    return async (dispatch) => {
        dispatch({type: SERVICOS.select, servicos: servico});
    }
}

export const getServicoById = (id) => {
    return async (dispatch) => {
        const result = await getServicoByIdService(id);
        dispatch({type: SERVICOS.select, servicos: result.data});
    }
}