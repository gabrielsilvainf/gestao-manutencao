import { getAvaliacoesService } from "../../services/avaliacoes.service";
import { AVALIACOES } from "../types";

export const getAvaliacoes = () => {
    return async (dispatch) => {
        const result = await getAvaliacoesService();
        const variosResult = [];
        for (let i = 0; i < 10; i++) {
            variosResult.push(result.data[0])
        }
        
        dispatch({ type: AVALIACOES.getALL, avaliacoes: variosResult });
    }
}