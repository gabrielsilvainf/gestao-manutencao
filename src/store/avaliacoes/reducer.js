import { AVALIACOES } from "../types";

const INITIAL_STATE = {
    todos: []
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type){
        case AVALIACOES.getALL:
            state.todos = action.avaliacoes;
            return state;
        default:
            return state;
    }
};

export default reducer;