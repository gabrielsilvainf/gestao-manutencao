export const SERVICOS = {
    getALL: "GET_ALL_SERVICOS",
    select: "SERVICO_SELECTED"
};

export const AUTH = {
    login: "AUTH_LOGIN",
    loading: "AUTH_LOADING"
};

export const AVALIACOES = {
    getALL: "GET_ALL_AVALIACOES"
};

export const PRODUTOS = {
    getALL: "GET_ALL_PRODUTOS",
    select: "PRODUTO_SELECTED"
};