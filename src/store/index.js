import { combineReducers, applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import servicosReducer from "./servicos/reducer";
import avaliacoesReducer from "./avaliacoes/reducer";
import produtosReducer from "./produtos/reducer";
import authReducer from "./auth/reducer";
import { composeWithDevTools } from "redux-devtools-extension";


const reducers = combineReducers({
    servicos: servicosReducer,
    auth: authReducer,
    avaliacoes: avaliacoesReducer,
    produtos: produtosReducer,
});

const middlewares = [thunk];

const compose = composeWithDevTools(applyMiddleware(...middlewares));

const store = createStore(reducers, compose);

export default store;