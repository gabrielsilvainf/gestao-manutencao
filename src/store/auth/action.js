import { navigate } from "@reach/router";
import http from "../../config/http";
import { saveAuth } from "../../config/storage";
import { authServiceLogin } from "../../services/auth.service"
import { AUTH } from "../types";
import { toast } from "react-toastify";

export const authLogin = (form) => {
    const routeLogin = {
        authenticated: "/admin",
        usuario: "/cliente",
    };
    
    return async (dispatch) => {
        dispatch({ type: AUTH.loading, status: true });
        
        try{
            const result = await authServiceLogin(form);
            if(result.data){
                saveAuth(result.data);
                http.defaults.headers['Authorization'] = `bearer ${result.data.jwt}`;
                dispatch({type: AUTH.login, data: result.data});
                const role = result.data.user.role.type;
                navigate(routeLogin[role]);
            }
            
        } catch (error) {
            dispatch({type: AUTH.loading, status: false});
            toast.error('Não foi possível efetuar o login, tente novamente');
        }
    }
}