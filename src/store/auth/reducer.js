import { getToken } from "../../config/storage";
import { AUTH } from "../types";

const INITIAL_STATE = {
    auth: getToken() || {},
    loading: false,
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case AUTH.loading:
            state.loading = action.status;
            return state;
        case AUTH.login:
            state.loading = false;
            state.auth = action.data;
            return state;
        default:
            return state;
    }
};

export default reducer;