import { PRODUTOS } from "../types";

const INITIAL_STATE = {
    todos: [],
    selecionado: {}
};

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type){
        case PRODUTOS.getALL:
            state.todos = action.produtos;
            return state;
        case PRODUTOS.select:
            state.selecionado = action.produtos;
            return state;
        default:
            return state;
    }
};

export default reducer;