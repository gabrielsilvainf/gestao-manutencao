import { getProdutosByIdService, getProdutosService } from "../../services/produtos.service";
import { PRODUTOS } from "../types";

export const getProdutos = () => {
    return async (dispatch) => {
        const result = await getProdutosService();
        dispatch({ type: PRODUTOS.getALL, produtos: result.data });
    }
}

export const produtoSelecionado = (produto) => {
    return async (dispatch) => {
        dispatch({type: PRODUTOS.select, produtos: produto});
    }
}

export const getProdutoById = (id) => {
    return async (dispatch) => {
        const result = await getProdutosByIdService(id);
        dispatch({type: PRODUTOS.select, produtos: result.data});
    }
}