import Admin from "./views/admin";
import Portal from "./views/portal";
import Cliente from "./views/cliente";
import { Redirect, Router } from "@reach/router";
import Funcionarios from "./views/funcionarios";
import "./config/bootstrap"
import { isAuthenticated } from "./config/storage";
import { useSelector } from "react-redux";
import { enumRole } from "./util/roles";
import Error404 from "./views/error/error404";

const Routers = () => {

    const userRole = useSelector(state => state.auth.auth?.user?.role.type);

    //pega o tipo da role
    const roleId = enumRole(userRole);
    
    // forma de entrar os componentes de maneira funcional
    const PrivateRoute = ({component: Component, ...rest}) => {
        if(!isAuthenticated()){
            return <Redirect to="/login" noThrow/>;
        }

        if(userRole && rest.type !== roleId){
            return <Error404/>;
        }

        return <Component {...rest}/>;
    }


    return(
        <Router>
            <Portal path="/*"/>

            {/* middleware */}
            <PrivateRoute 
                component={Admin} 
                path="/admin/*"
                perfil="ADMIN"
                type={1}
            />

            <PrivateRoute 
                component={Cliente} 
                path="/cliente/*"
                perfil="cliente"
                type={3}
            />

            <PrivateRoute 
                component={Funcionarios} 
                path="/funcionarios/*"
                perfil="funcionario"
                type={2}
            />
        </Router>
    );
}

export default Routers;