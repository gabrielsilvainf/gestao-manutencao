import http from "../config/http";

const path = "/lojas";

export const getProdutosService = () => http.get(`${path}`);

export const getProdutosByIdService = (id) => http.get(`${path}/${id}`);

export const deleteProdutosService = (id) => http.delete(`${path}/${id}`);