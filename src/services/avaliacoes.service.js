import http from "../config/http";

const path = "/avaliacoes";

export const getAvaliacoesService = () => http.get(`${path}`);