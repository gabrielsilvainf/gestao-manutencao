import http from "../config/http";

const path = '/servicos'

export const getServicoService = () => http.get(`${path}`);

export const getServicoByIdService = (id) => http.get(`${path}/${id}`);

export const deleteService = (id) => http.delete(`${path}/${id}`);