import { createGlobalStyle } from "styled-components";

const GlobalStyled = createGlobalStyle`
    *{
        margin:0;
        padding:0;
        outline: 0;
    }

    html{
        height: 100vh
    }
`;

export default GlobalStyled;